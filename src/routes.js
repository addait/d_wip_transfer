import Dashboard from "./views/Dashboard/Dashboard";
import Transfer from "./views/Transfer/Transfer";
import List from "./views/List/List";

const routes = [
  { path: "/", exact: true, name: "หน้าหลัก" },
  { path: "/dashboard", exact: true, name: "Dashboard", component: Dashboard },
  {
    path: "/transfer/add",
    exact: true,
    name: "เพิ่มใบโอนงาน",
    component: Transfer,
  },
  {
    path: "/transfer/get",
    exact: true,
    name: "รายการโอนงาน",
    component: List,
  },
];

export default routes;
