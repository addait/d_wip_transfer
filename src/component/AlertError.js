import React from "react";
import Swal from "sweetalert2";


class AlertError extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return ( 

            Swal.fire({
                icon: "error",
                title:"ข้อผิดพลาด !",
                text: this.props.err,
                showConfirmButton: true,
                confirmButtonText: 'ตกลง'
                })

        )
    }

}

export default AlertError;