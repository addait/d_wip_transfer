import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Spinner from "react-spinkit";
import "./App.scss";

import Layout from "./containers/Layout";
import Login from "./views/Login/Login";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">
    <Spinner name="three-bounce" />
  </div>
);
class App extends React.Component {
  render() {
    return (
      // basename="/wip"
      <BrowserRouter basename="/transfer">
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route
              exact
              path="/login"
              name="Login"
              render={(props) => <Login {...props} />}
            />

            <Route
              path="/"
              name="Home"
              render={(props) => <Layout {...props} />}
            />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
