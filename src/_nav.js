export default {
    items:[
      {
        title: true,
        name: 'รายการประจำวัน',
        wrapper: {            // optional wrapper object
          element: '',        // required valid HTML5 element tag
          attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
        },
        class: ''             // optional class names space delimited list for title item ex: "text-center"
      },
        {
            name: 'เพิ่มใบโอนงาน',
            url: '/transfer/add',
            icon: 'icon-note',
            attributes: { disabled: false }
        },
        {
          name: 'รายการโอนงาน',
          url: '/transfer/get',
          icon: 'icon-list',
          attributes: { disabled: false },
          // variant: 'success',    
        }
    ]
}