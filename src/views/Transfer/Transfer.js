import React from "react";

import { Card, CardBody, Col, Row } from "reactstrap";
import { Form, Input, Divider, Radio, Tag, Switch, Table } from "antd";
import Cookies from "js-cookie";
import { URL, ID, Token, Supplier, AlertAuthen } from "../../config";
import FocusLock from "react-focus-lock";
import numeral from "numeral";

import {
  BarcodeOutlined,
  CloseOutlined,
  CheckOutlined,
} from "@ant-design/icons";
import Axios from "axios";
import Swal from "sweetalert2";
import alasql from "alasql";
const english = /^[A-Z\a-z\0-9\-]*$/;

class Transfer extends React.Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.ScanRef = React.createRef();
    this.state = {
      number: 0,
      display: false,
      data: [],
      datalog: [],
      mac: "",
      valid: "",
      scanbarcode: false,
      loadingTable: false,
      columns: [
        {
          title: "No.",
          dataIndex: "no",
          key: "no",
          align: "center",
          width: 70,
          render: (text, record) => numeral(text).format("0,0"),
        },
        {
          title: "P/I",
          dataIndex: "referencePiNo",
          key: "referencePiNo",
          // align: "center",
          width: 200,
        },
        {
          title: "รหัสชิ้นงาน",
          dataIndex: "itemCode",
          key: "itemCode",
          width: 250,
        },
        {
          title: "รายการ",
          dataIndex: "name",
          key: "name",
          // align: "center",
          ellipsis: true,
        },
        {
          title: "จำนวน",
          dataIndex: "qty",
          key: "qty",
          align: "center",
          width: 100,
          render: (text, record) => numeral(text).format("0,0"),
        },
        {
          title: "หน่วย",
          dataIndex: "unit",
          key: "unit",
          align: "center",
          width: 80,
        },
      ],
    };
  }

  handleScan = (data) => {
    console.error(data);
  };
  handleError = (err) => {
    console.error(err);
  };

  onReset = () => {
    console.log("times", this.formRef.current.getFieldValue("times"));
    console.log("mac", this.formRef.current.getFieldValue("mac"));
    // this.formRef.current.resetFields();
  };

  componentDidMount = async () => {
    const display = window.innerWidth <= 600 ? true : false;
    await this.setState({ display });
  };

  onFormLayoutChange = ({ mac }) => {
    this.setState({ mac });
  };

  onBlurMachine = async () => {
    if (this.state.mac === "") {
      this.setState({ valid: "", mac: "" });
      this.formRef.current.setFieldsValue({
        mac: "",
      });
    } else {
      await Axios.get(URL + "/machine/check/" + this.state.mac, {
        headers: { authorization: Cookies.get(ID) + ":" + Cookies.get(Token) },
      })
        .then(async (res) => {
          // console.log("res", res);
          if (res.data.success) {
            //   console.log(res);
            if (res.data.data) {
              await this.setState({ valid: "success" });
            } else {
              await this.setState({ valid: "", mac: "" });
              this.formRef.current.setFieldsValue({
                mac: "",
              });
              // console.log(this.formRef.current);
            }
          } else {
            AlertAuthen(this.props.history.push);
          }
        })
        .catch((err) => console.log(err));
    }
  };

  onEnter = async () => {
    const txt = this.ScanRef.current.getFieldValue("barcode");

    if (txt) {
      if (txt.trimLeft() !== "") {
        if (english.test(txt)) {
          await this.CheckRepeat(txt);
        } else {
          Swal.fire({
            icon: "warning",
            title: "แจ้งเตือน !",
            text: "กรุณาเปลี่ยนเป็นภาษาอังกฤษ !",
            confirmButtonText: "ตกลง",
          });
        }
      }
    }
    this.ScanRef.current.resetFields();
  };

  onChangeSwitch = (checked) => {
    // console.log(checked);
    this.setState({ scanbarcode: checked });
  };

  CheckRepeat = async (txt) => {
    await Axios.post(
      URL + "/transaction/receive/check_barcode",
      {
        barcode: txt,
        supplierID: Cookies.get(Supplier) === null ? Cookies.get(Supplier) : "",
      },
      {
        headers: { authorization: Cookies.get(ID) + ":" + Cookies.get(Token) },
      }
    )
      .then(async (res) => {
        // console.log("res", res);
        if (res.data.success) {
          if (res.data.data.status === "failed") {
            Swal.fire({
              icon: "error",
              title: "ข้อผิดพลาด !",
              text: res.data.data.message,
              confirmButtonText: "ตกลง",
            });
          } else {
            const data = [...this.state.datalog, res.data.data.po];
            const duplicate = alasql(
              `select barcode from ? where barcode = '${res.data.data.po.barcode}' `,
              [this.state.datalog]
            );
            const show = alasql(
              `select referencePiNo,itemCode,name,SUM(qty) as qty,unit from ? group by referencePiNo,itemCode,name,unit`,
              [data]
            );
            show.forEach((r, i) => {
              r.no = i + 1;
            });
            if (duplicate.length > 0) {
              Swal.fire({
                icon: "warning",
                title: "แจ้งเตือน !",
                text: res.data.data.po.barcode + " ซ้ำ !",
                confirmButtonText: "ตกลง",
              });
            } else {
              this.setState({ datalog: data, data: show });
            }
          }
        } else {
          AlertAuthen(this.props.history.push);
        }
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <Row
                // style={{ borderBottom: "1px solid #c8ced3" }}
                >
                  <Col>
                    <Form
                      //   {...formItemLayout}
                      layout={"inline"}
                      ref={this.formRef}
                      name="control-ref"
                      initialValues={{ times: "a", mac: this.state.mac }}
                      onValuesChange={this.onFormLayoutChange}
                    >
                      <Form.Item name="times" label="กะ">
                        <Radio.Group>
                          <Radio.Button value="a">กะ 1</Radio.Button>
                          <Radio.Button value="b">กะ 2</Radio.Button>
                        </Radio.Group>
                      </Form.Item>

                      <Form.Item
                        name="mac"
                        label="รหัสเครื่องจักร"
                        hasFeedback
                        validateStatus={this.state.valid}
                        maxLength={10}
                        rules={[
                          {
                            required: false,
                            // message: "กรุณากรอก รหัสเครื่องที่ !",
                          },
                        ]}
                      >
                        <Input
                          autoComplete={"off"}
                          onBlur={this.onBlurMachine}
                          placeholder="กรอกรหัสเครื่องจักร"
                        />
                      </Form.Item>

                      <Form.Item name="psfID" label="รหัสพนักงาน">
                        <Input
                          autoComplete={"off"}
                          maxLength={5}
                          placeholder="กรอกรหัสพนักงาน"
                        />
                      </Form.Item>
                    </Form>
                    <br />
                    <Form ref={this.ScanRef} style={{ marginBottom: 0 }}>
                      <Form.Item
                        name="switch"
                        label="ยิงบาร์โค้ด"
                        style={{ marginBottom: 0 }}
                      >
                        <Switch
                          checked={this.state.scanbarcode}
                          checkedChildren={<CheckOutlined />}
                          unCheckedChildren={<CloseOutlined />}
                          onChange={this.onChangeSwitch}
                        />
                      </Form.Item>

                      <FocusLock
                        disabled={!this.state.scanbarcode}
                        persistentFocus={true}
                      >
                        <Form.Item
                          name="barcode"
                          label="BARCODE"
                          rules={[
                            {
                              required: false,
                              // message: "กรุณากรอก รหัสพนักงาน !",
                            },
                          ]}
                          style={{ marginBottom: 0 }}
                        >
                          <Input
                            disabled={!this.state.scanbarcode}
                            addonAfter={<BarcodeOutlined />}
                            autoComplete={"off"}
                            onPressEnter={this.onEnter}
                          />
                        </Form.Item>
                      </FocusLock>
                    </Form>
                  </Col>
                  <Col
                    style={{
                      borderLeft: "1px solid #f0f0f0",
                    }}
                  >
                    <Row style={{ marginLeft: 10, marginTop: 10 }}>
                      <Tag icon={<BarcodeOutlined />} color="#f50">
                        จำนวนบาร์โค้ด
                      </Tag>
                    </Row>
                    <Row style={{ justifyContent: "center" }}>
                      <label style={{ fontWeight: "bold", fontSize: 100 }}>
                        {this.state.number}
                      </label>
                    </Row>
                  </Col>
                  {/* <Button onClick={this.onReset}>asdas</Button> */}
                </Row>
                <Divider style={{ marginBottom: 10 }} />
                <Table
                  rowKey="no"
                  dataSource={this.state.data}
                  columns={this.state.columns}
                  scroll={{ x: 500, y: 350 }}
                  size="small"
                  bordered
                  pagination={false}
                  loading={{
                    spinning: this.state.loadingTable,
                    size: "large",
                    tip: "กำลังโหลด...",
                  }}
                  locale={{ emptyText: "ไม่มีข้อมูล" }}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Transfer;
