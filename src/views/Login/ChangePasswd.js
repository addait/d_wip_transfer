import React from "react";
import Axios from "axios";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
  FormFeedback,
} from "reactstrap";
import { LoadingOutlined } from "@ant-design/icons";
import Swal from "sweetalert2";
import { URL, ID, Token } from "../../config";
import { withRouter } from "react-router-dom";

import Cookies from "js-cookie";

class ChangePasswd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      passwdOld: "",
      passwdNew: "",
      passwdConfirm: "",
      valid: false,
      btnable: false,
      invalid: false,
      pwOldValid: false,
      pwOldInvalid: false,
    };

    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  toggle() {
    this.setState({ valid: false, invalid: false });
    this.props.setModal(false);
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });

    if (e.target.name === "passwdNew") {
      if (e.target.value === "" || this.state.passwdConfirm === "") {
        return this.setState({ valid: false, invalid: false });
      }
      this.passwordMath(this.state.passwdConfirm, e.target.value);
    }

    if (e.target.name === "passwdConfirm") {
      if (e.target.value === "") {
        return this.setState({ valid: false, invalid: false });
      }
      this.passwordMath(this.state.passwdNew, e.target.value);
    }
  };

  passwordMath = (passwd1, passwd2) => {
    if (passwd1 === passwd2) {
      this.setState({ valid: true, invalid: false });
    } else {
      this.setState({ valid: false, invalid: true });
    }
  };

  onSubmit = async (e) => {
    e.preventDefault();

    if (this.state.valid === false && this.state.invalid === true) {
      return Swal.fire("รหัสผ่านไม่ตรงกัน", "กรุณาตรวจสอบ", "error");
    }
    this.setState({ btnable: true });
    await Axios.post(
      URL + "/users/change/password",
      {
        user_id: Cookies.get(ID),
        pass_old: this.state.passwdOld,
        pass_new: this.state.passwdConfirm,
      },
      { headers: { authorization: Cookies.get(ID) + ":" + Cookies.get(Token) } }
    )
      .then((res) => {
        // console.log(res);
        if (res.data.success) {
          if (res.data.data.status === "success") {
            Swal.fire({
              icon: "success",
              title: "รหัสผ่านถูกเปลี่ยนเรียบร้อย",
              confirmButtonText: "ตกลง",
            });
            this.toggle();
          } else {
            Swal.fire({
              icon: "error",
              title: "รหัสผ่านเก่าไม่ถูกต้อง",
              text: "โปรดลองใหม่อีกครั้ง",
              confirmButtonText: "ตกลง",
            }).then((result) => {
              if (result.value) {
                this.setState({ btnable: false });
              }
            });
          }
        }
      })
      .catch((err) => {
        this.setState({ btnable: false });
        console.error(err);
      });
  };

  close = () => {
    this.setState({
      passwdOld: "",
      passwdNew: "",
      passwdConfirm: "",
      valid: false,
      btnable: false,
      invalid: false,
      pwOldValid: false,
      pwOldInvalid: false,
    });
  };

  render() {
    return (
      <Modal
        isOpen={this.props.modal}
        toggle={this.toggle}
        className={"modal-sm "}
        onClosed={this.close}
      >
        <ModalHeader toggle={this.toggle}>เปลี่ยนรหัสผ่าน</ModalHeader>
        <Form onSubmit={(e) => this.onSubmit(e)} className="form-horizontal">
          <ModalBody>
            <FormGroup>
              <Label htmlFor="passwordOld">รหัสผ่านเก่า</Label>
              <Input
                type="password"
                id="passwdOld"
                name="passwdOld"
                onChange={(e) => this.handleChange(e)}
                required
                autoFocus
                valid={this.state.pwOldValid}
                invalid={this.state.pwOldInvalid}
              />
              <FormFeedback>รหัสผ่านไม่ถูกต้อง</FormFeedback>
            </FormGroup>

            <FormGroup>
              <Label htmlFor="passwordNew">รหัสผ่านใหม่</Label>
              <Input
                type="password"
                id="passwdNew"
                name="passwdNew"
                onChange={(e) => this.handleChange(e)}
                required
              />
            </FormGroup>

            <FormGroup>
              <Label htmlFor="passwordConfirm">ยืนยันรหัสผ่านใหม่</Label>
              <Input
                type="password"
                id="passwdConfirm"
                name="passwdConfirm"
                onChange={(e) => this.handleChange(e)}
                required
                valid={this.state.valid}
                invalid={this.state.invalid}
              />
              <FormFeedback>รหัสผ่านไม่ตรงกัน</FormFeedback>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="primary" disabled={this.state.btnable}>
              {this.state.btnable ? (
                <div style={{ display: "flex" }}>
                  <LoadingOutlined style={{ alignSelf: "center" }} />{" "}
                  {" กำลังโหลด..."}
                </div>
              ) : (
                "ยืนยันรหัสผ่านใหม่"
              )}
            </Button>{" "}
            <Button
              color="secondary"
              onClick={this.toggle}
              disabled={this.state.btnable}
            >
              ยกเลิก
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

export default withRouter(ChangePasswd);
