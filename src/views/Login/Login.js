import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert,
} from "reactstrap";
import Cookies from "js-cookie";
import { fetchAPI } from "../../module";
import { withRouter } from "react-router-dom";
import { Token, ID, Supplier } from "../../config";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userID: "",
      pass: "",
      success: "",
      msg: null,
      isLoading: false,
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      success: "",
    });
  };

  onSubmit = async (e) => {
    e.preventDefault();
    this.setState({ isLoading: true });

    const params = {
      user_id: this.state.userID,
      pass: this.state.pass,
    };

    const res = await fetchAPI("post", "/login/tranfer", params);
    // console.log(res);

    if (res.status === "success") {
      Cookies.set(Supplier, res.data.supplier_id);
      Cookies.set(Token, res.data.access_token);
      Cookies.set(ID, res.data.user_id);

      this.setState({ success: true, isLoading: false });
      this.props.history.push("/");
    } else {
      this.setState({
        success: false,
        msg: res.data.message,
        isLoading: false,
      });
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={(e) => this.onSubmit(e)}>
                      {/* {internalIp.v4.sync()} */}
                      <h1>WIP โอนงาน </h1>
                      {/* <img src={`${process.env.PUBLIC_URL}/img/LogoCRP.png`} className="img-fluid" alt="ADDA CRP" /> */}
                      <p className="text-muted">
                        ระบบโอนงานระหว่างแผนก (WIPNEW)
                      </p>

                      {this.state.success === false ? (
                        <Alert
                          className="alert alert-danger fade show"
                          variant="danger"
                        >
                          {this.state.msg}
                        </Alert>
                      ) : (
                        ""
                      )}

                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          name="userID"
                          placeholder="ชื่อผู้ใช้งาน"
                          autoComplete="off"
                          autoFocus
                          required
                          onChange={(e) => this.handleChange(e)}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="pass"
                          placeholder="รหัสผ่าน"
                          autoComplete="current-password"
                          required
                          onChange={(e) => this.handleChange(e)}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="12">
                          <Button
                            type="submit"
                            color="primary"
                            className="px-4 btn-lg btn-block btn-facebook"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading
                              ? "กำลังโหลด..."
                              : "เข้าสู่ระบบ"}
                          </Button>
                        </Col>
                        {/* <Col xs="6" className="text-right">
                          <Button
                            disabled={this.state.isLoading}
                            color="link"
                            className="px-0"
                          >
                            ลืมรหัสผ่าน?
                          </Button>
                        </Col> */}
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
