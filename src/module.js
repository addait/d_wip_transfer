import Cookies from "js-cookie";
import Axios from "axios";

import { URL, Token, ID } from "./config";

const signRequest = () => {
  return Cookies.get(ID) + ":" + Cookies.get(Token);
};

export const fetchAPI = async (method, endpoint, prams = {}) => {
  if (method === "post") {
    const result = await Axios.post(URL + endpoint, prams, {
      headers: { authorization: signRequest() },
    });

    return result.data;
  } else if (method === "get") {
    const result = await Axios.get(URL + endpoint, {
      headers: { authorization: signRequest() },
    });

    return result.data;
  } else {
    return {};
  }
};
