import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import * as router from "react-router-dom";
import { Container } from "reactstrap";
import Spinner from "react-spinkit";
import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav2 as AppSidebarNav,
  AppBreadcrumb2 as AppBreadcrumb,
} from "@coreui/react";

// sidebar nav config
import navigation from "../../_nav";
// routes config
import routes from "../../routes";

import Header from "./Header";
import Footer from "./Footer";

import Cookies from "js-cookie";
import Axios from "axios";
import { Token } from "../../config";

import { removeauthen, URL, ID } from "../../config";

class Layout extends React.Component {
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">
      <Spinner name="three-bounce" />
    </div>
  );

  onLogout = async () => {
    await Axios.post(
      URL + "/users/logout",
      {
        user_id: Cookies.get(ID),
      },
      { headers: { authorization: Cookies.get(ID) + ":" + Cookies.get(Token) } }
    )
      .then(async (res) => {
        // console.log("logout", res);
        if (res.data.data.status === "success") {
          await removeauthen();
          this.props.history.push("/login");
        }
      })
      .catch((err) => console.log(err));
  };

  signOut = async (e) => {
    e.preventDefault();
    await this.onLogout();
  };

  render() {
    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <Header onLogout={(e) => this.signOut(e)} />
          </Suspense>
        </AppHeader>

        <div className="app-body">
          <AppSidebar
            fixed
            display="lg"
            minimized={false}
            offCanvas={true}
            compact={false}
            isOpen={false}
          >
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav
                navConfig={navigation}
                {...this.props}
                router={router}
                isOpen={true}
              />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} router={router} />
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      Cookies.get(Token) ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={(props) => <route.component {...props} />}
                        />
                      ) : (
                        <Redirect key={idx} to="/login" />
                      )
                    ) : null;
                  })}

                  <Redirect from="/" to="/" />
                </Switch>
              </Suspense>
            </Container>
          </main>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <Footer />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default Layout;
