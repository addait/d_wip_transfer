import React from "react";
import { withRouter } from "react-router-dom";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
} from "reactstrap";
import PropTypes from "prop-types";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";

import logo from "../../assets/img/brand/logoWipTransfer.svg";
import logoSmall from "../../assets/img/brand/logoWipTransferSmall.svg";

import ChangePasswd from "../../views/Login/ChangePasswd";

import { AlertAuthen, ID, Token, URL } from "../../config";

// import { fetchAPI } from "../../module";

import Cookies from "js-cookie";

import Axios from "axios";

const propTypes = {
  children: PropTypes.node,
};

const mql = window.matchMedia(`(min-width: 600px)`);

const defaultProps = {};

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fullname: "",
      permiss: true,
      modal: false,
      display: 0,
    };
  }

  componentDidMount = async () => {
    mql.addListener(this.mediaQueryChanged);
    if (Cookies.get(Token)) {
      await this.CheckPermiss();
    }
  };

  mediaQueryChanged = async () => {
    this.setState({ display: mql.matches });
  };

  CheckPermiss = async () => {
    const path = "/users/profile/" + Cookies.get(ID);
    await Axios.get(URL + path, {
      headers: { authorization: Cookies.get(ID) + ":" + Cookies.get(Token) },
    })
      .then((res) => {
        if (res.data.success) {
          if (res.data.data.user_level === "A") {
            this.setState({
              permiss: false,
            });
          } else {
            this.setState({
              permiss: true,
            });
          }
          this.setState({ fullname: res.data.data.full_name });
        } else {
          AlertAuthen(this.props.history.push);
        }
      })
      .catch((err) => console.log(err));
  };

  changePage = (path) => {
    return this.props.history.push(path);
  };

  setModel = (modal) => {
    this.setState({ modal });
  };

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={
            mql.matches
              ? { src: logo, width: 210, height: 30, alt: "WIP Logo" }
              : null
          }
          minimized={{
            src: logoSmall,
            width: 45,
            height: 45,
            alt: "WIP Logo",
          }}
        ></AppNavbarBrand>

        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <label style={{ fontWeight: "bold" }}>
                {this.state.fullname}
              </label>
              <img
                src={process.env.PUBLIC_URL + "/img/7.jpg"}
                className="img-avatar"
                alt="admin"
              />
            </DropdownToggle>

            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>ตั้งค่า</strong>
              </DropdownItem>
              <DropdownItem onClick={() => this.setModel(true)}>
                <i className="fa fa-key"></i> เปลี่ยนรหัสผ่าน
              </DropdownItem>

              <DropdownItem onClick={(e) => this.props.onLogout(e)}>
                <i className="fa fa-lock"></i> ออกจากระบบ
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        <ChangePasswd modal={this.state.modal} setModal={this.setModel} />
      </React.Fragment>
    );
  }
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default withRouter(Header);
