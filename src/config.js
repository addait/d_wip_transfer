import Cookies from "js-cookie";
import Swal from "sweetalert2";

export const URL = "http://10.32.0.84/wip/api";
// export const URL = "http://10.32.1.154:1080/api";

export const Supplier = "SupTransfer";
export const Token = "TokenTransfer";
export const ID = "UserTransfer";

export const removeauthen = () => {
  Cookies.remove(Supplier);
  Cookies.remove(Token);
  Cookies.remove(ID);
};

export const AlertAuthen = (back) => {
  Swal.fire({
    icon: "error",
    title: "ข้อผิดพลาด !",
    text: "ผู้ใช้งานท่านอื่นกำลังเข้าสู่ระบบรหัสนี้...",
    showConfirmButton: true,
    confirmButtonText: "ตกลง",
  }).then((result) => {
    if (result.value) {
      removeauthen();
      back("/login");
    } else if (result.dismiss) {
      removeauthen();
      back("/login");
    }
  });
};
